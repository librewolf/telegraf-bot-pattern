module.exports = bot => {
	const { startTask } = bot.handlers

	bot.command('start', startTask.start)

	const routeList = ctx => {
		switch (String(ctx.state.value)) {
			case 'test':
				return startTask.subscribe(ctx)
		}
	}

	bot.route.on('startMenu', routeList)
}
