const { readdirSync } = require('fs')
const path = require('path')
const ms = require('ms')
const oS = require('os')
const cS = require('chalk')

const config = {}

config.isDev = process.env.NODE_ENV === 'development'

config.sessionStore = db => {
	return {
		store: {
			get: key => db.Session.findOne({ key }).sort({ key_1: 1 }).lean(),
			set: (key, session) =>
				db.Session.updateOne({ key }, { $set: session }, { upsert: true })
					.sort({ key_1: 1 })
					.lean()
		}
	}
}
config.consignOpts = {
	cwd: process.cwd() + '/src',
	extensions: ['.js'],
	verbose: false
}

config.limitSecurity = {
	window: ms('15s'),
	limit: 6,
	onLimitExceeded: ({ replyWithHTML }) =>
		replyWithHTML('<code>❗️Please wait a second (5s)</code>', {
			disable_notification: true
		})
}

config.i18nOpts = {
	directory: path.resolve(__dirname, '../locales'),
	defaultLanguage: 'en',
	defaultLanguageOnMissing: true
}

config.route = ({ callbackQuery }) => {
	if (!callbackQuery.data) return
	const [route, value] = callbackQuery.data.split('|')
	return {
		route,
		state: {
			value
		}
	}
}

config.settings = {
	setCommand: ({ setMyCommands }, next) => {
		return next().then(() => {
			setMyCommands([
				{
					command: 'start',
					description: 'Start bot or restart'
				},
				{
					command: 'lang',
					description: 'Change language'
				}
			])
		})
	}
}

module.exports = {
	config,
	readdirSync,
	path,
	ms,
	oS,
	cS
}
