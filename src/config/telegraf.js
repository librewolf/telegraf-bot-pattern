const { Telegraf, Router, Markup, Extra, session } = require('telegraf')
const { db } = require('../db/db')
const { I18n } = require('telegraf-i18n')
const { config } = require('./config')
const { rateLimit, reportErr, updaterUser } = require('../middle')
const {
	generateUpdateMiddleware: cliUpdateTime
} = require('telegraf-middleware-console-time')
const consign = require('consign')

const bot = new Telegraf(process.env.BOT_TOKEN, {
	handlerTimeout: 1,
	telegram: {
		apiRoot: process.env.TG_APIROOT
	}
})

bot.context = {
	db,
	extra: Extra,
	markup: Markup
}

bot.use(rateLimit(config.limitSecurity))
bot.use(session(config.sessionStore(db)))
bot.use(new I18n(config.i18nOpts))
bot.use(updaterUser)
bot.use(reportErr)
;(async () => {
	if (config.isDev) {
		const { TG_APIROOT, BOT_CLUSTER_CORE } = process.env
		bot.use(Telegraf.log())
		bot.use(cliUpdateTime())
		await bot.use(config.settings.setCommand)
		console.log(
			Object.assign(await bot.telegram.getMe(), {
				BOT_CLUSTER_CORE,
				TG_APIROOT
			})
		)
	}
})()

const Route = new Router(config.route)
bot.route = Route

bot.on('callback_query', Route)
bot.route.otherwise(({ deleteMessage }) => deleteMessage())

consign(config.consignOpts).include('handlers').then('route').into(bot)

bot.use(({ reply, i18n, message }) =>
	reply(
		i18n.t('default.noAnswer'),
		Extra.HTML()
			.inReplyTo(message.message_id)
			.markup(m =>
				m.inlineKeyboard([
					m.callbackButton(i18n.t('default.closeMessage'), 'delete')
				])
			)
	)
)

module.exports = { bot }
