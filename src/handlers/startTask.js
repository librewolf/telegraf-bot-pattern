class StartTask {
	async start({ reply, replyWithSticker, message, i18n, from, extra }) {
		await replyWithSticker(
			'CAACAgIAAxkBAAIBs2DeqZIZBIYgeFBsno36780WxeHsAAIQAAPDVgMednAUKnTod-sgBA'
		)
		const markup = extra
			.HTML()
			.inReplyTo(message.message_id)
			// .webPreview(false)
			.markup(m =>
				m.inlineKeyboard(
					[
						m.urlButton('Subscribe', 'https://t.me/ThisOpenSource'),
						m.callbackButton('Subscribe ACQ', 'startMenu|test'),
						m.urlButton('❤️ My Notabug', 'https://notabug.org/Secven'),
						m.callbackButton('❌ Delete', 'delete')
					],
					{ columns: 2 }
				)
			)
		return await reply(i18n.t('main.start', { ...from }), markup)
	}

	subscribe = async ctx => {
		return await ctx.answerCbQuery('https://t.me/ThisOpenSource', true)
	}
}

module.exports = new StartTask()
