const { escapeHtml } = require('../util/escapeHTML')

module.exports = async ({ telegram, updateType, i18n }, next) => {
	const noop = () => {}
	const { username } = await telegram.getMe()
	return next().catch(async err => {
		const __TEMPLATE__ = {
			type: updateType,
			error: escapeHtml(err.message),
			stack: escapeHtml(err.stack),
			botName: `@${username}`
		}
		await telegram
			.sendMessage(
				process.env.ADMIN_ID,
				i18n.t('stackError.report', { ...__TEMPLATE__ }),
				{
					parse_mode: 'HTML',
					disable_web_page_preview: true
				}
			)
			.catch(noop)
	})
}
