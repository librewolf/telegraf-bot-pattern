const { escapeHtml } = require('../util/escapeHTML')

module.exports = async ({ db, from, i18n, session }, next) => {
	// eslint-disable-next-line camelcase
	const { id: user_id, first_name, username } = from
	// eslint-disable-next-line camelcase
	if (!user_id) return await next()
	await db.User.findOne({ user_id })
		.sort({ user_id_1: 1 })
		.then(async root => {
			const nameFirst = escapeHtml(first_name) ?? 'Unknown'
			if (!root) {
				root = new db.User()
				// eslint-disable-next-line camelcase
				root.user_id = user_id
				root.first_name = nameFirst
				root.username = username ?? 'Unknown'
			}

			root.first_name = nameFirst
			root.username = username ?? 'Unknown'

			session.root = root

			i18n.locale(session.root.settings.lang)

			return next().then(async () => {
				await session.root.save()
			})
		})
		.catch(err => console.log(err.message))
}
