const __MODELS__ = {
	User: require('./model/User'),
	Session: require('./model/Session')
}

const db = {}

Object.keys(__MODELS__).forEach(model => {
	return (db[model.toString()] = __MODELS__[model.toString()])
})

module.exports = { db }

/*
readdirSync(path.resolve(__dirname, MODEL_PATTERN)).forEach(model => {
	const setModels = model.split('.')[0].toString().trim().match(DEFAULT_CHECK)
	if (!setModels)
		throw new Error(`Error: secure loaded ${MODEL_PATTERN}${model.trim()}`)
	// eslint-disable-next-line security/detect-non-literal-require
	db[setModels.toString()] = require(MODEL_PATTERN + setModels)
})
*/
