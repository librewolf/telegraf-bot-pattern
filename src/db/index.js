const mongoose = require('mongoose')
const { config, cS } = require('../config/config')

class Mongoose {
	async mongooseConnect() {
		await mongoose
			.connect(process.env.MONGOOSE_URL, {
				keepAlive: true,
				keepAliveInitialDelay: 300000,
				useCreateIndex: true,
				useNewUrlParser: true,
				useUnifiedTopology: true,
				socketTimeoutMS: 45000,
				connectTimeoutMS: 35000
			})
			.catch(err => {
				console.error(err.stack)
				console.error(
					cS.red.bold(`Mongoose error: ${process.env.MONGOOSE_URL}`)
				)
				process.exit(0)
			})

		mongoose.set('debug', config.isDev)

		process.on('SIGINT', () => {
			mongoose.connection.close(() => {
				console.log(`Disconnect ${process.env.MONGOOSE_URL}`)
				process.exit(0)
			})
		})
	}
}

module.exports = new Mongoose()
