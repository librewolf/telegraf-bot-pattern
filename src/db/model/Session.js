const { Schema, model } = require('mongoose')

const schemaSession = new Schema(
	{
		key: {
			type: String,
			unique: true,
			required: true,
			index: true
		},
		session: {},
		expires: Date
	},
	{
		timestamps: { createdAt: true, updatedAt: true }
	}
)

module.exports = model('Session', schemaSession)
