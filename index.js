process.on('unhandledRejection', err => console.error(err.message))
process.on('uncaughtException', err => console.error(err.stack))
require('dotenv').config({ path: './.env' })
require('./src/app')
