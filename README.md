## Telegraf Bot Pattern

TEST BOT => [@patternRobot](https://t.me/patternRobot)

![example](https://i.imgur.com/5lbe3pl.png)

#### Change .env.example to .env

> Setup bot: `npm i pm2 -g` `npm i yarn -g` 

> Installing packages: `yarn install`

> Mode Default: `yarn dev` - Development

> Mode Cluster: `yarn prod` - Production bot

> Mode Production PM2: `yarn pm:prod` `yarn pm:stop` `yarn pm:up` // pm2 startup

> Example `.env` ( BOT_CLUSTER_CORE = 2 ) or AutoScale ( BOT_CLUSTER_CORE = 0 )
