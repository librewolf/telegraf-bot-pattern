module.exports = {
	apps: [
		{
			name: 'APP-BOT',
			script: './index.js',
			max_memory_restart: '1000M',
			wait_ready: true,
			cron_restart: '30 2 * * *',
			env_production: {
				NODE_ENV: 'production'
			}
		}
	]
}
